package com.shopify.api.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.BadRequestException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.shopify.api.client.ShopifyClient;
import com.shopify.api.common.Credential;
import com.shopify.api.resources.CustomCollection;
import com.shopify.api.services.CustomCollectionsService;

public class ServiceTests {

	private ShopifyClient client;
	
	@Before
	public void setupLogging() {
		
		/*
		shopify.apiKey=813f493c07bbd41beca155184701d134
		shopify.sharedSecret=77bdea2f2d097eda50045aee144d3236
		shopify.shopName=footballhelmets-com
		shopify.apiPassword=f23f241484246226e1703c2f69e066bb
		 */
		
		Credential creds = new Credential("813f493c07bbd41beca155184701d134", "77bdea2f2d097eda50045aee144d3236", "footballhelmets-com", "f23f241484246226e1703c2f69e066bb");
		client = new ShopifyClient(creds);
	}
	
	@Test
	public void testGetCollections() throws IOException{
		List<CustomCollection> collections = client.constructInterface(CustomCollectionsService.class).getCustomCollections();
		Assert.assertTrue(!collections.isEmpty());
	}
	
	@Test
	public void testGetPaginatedCollections() throws IOException{
		CustomCollectionsService svc = client.constructInterface(CustomCollectionsService.class);
		
		List<CustomCollection> collections = new ArrayList<CustomCollection>();
		int page = 0;
		while(true){
			List<CustomCollection> newCollections = svc.getCustomCollections("page="+page);
			if(!collections.addAll(newCollections)) break; //false means the collection didn't change, so the page was empty
			page++;
		}
		
		Assert.assertTrue(!collections.isEmpty());
	}
	
	@Test
	public void testGetTwoCollections() throws IOException{
		CustomCollectionsService svc = client.constructInterface(CustomCollectionsService.class);
		
		CustomCollection collection1 = svc.getCustomCollection(12833757);
		CustomCollection collection2 = svc.getCustomCollection(12833541);
		Assert.assertTrue(collection2.getId()!=collection1.getId());
	}
	
	@Test
	public void testCreateCollection() throws IOException{
		try {
			CustomCollection collection = client.constructInterface(CustomCollectionsService.class).createCustomCollection(new CustomCollection().setTitle("Test 123"));
			Assert.assertTrue(collection.getId()>0);
			client.constructInterface(CustomCollectionsService.class).deleteCustomCollection(collection.getId());
		} catch (BadRequestException e) {
			throw new RuntimeException(e.getResponse().readEntity(String.class));
		}
	}
	
	@Test
	public void testCountCollections() throws IOException{
		try {
			int count = client.constructInterface(CustomCollectionsService.class).getCount();
			Assert.assertTrue(count>0);
		} catch (BadRequestException e) {
			throw new RuntimeException(e.getResponse().readEntity(String.class));
		}
	}
	
	@Test
	public void testSearchCollections() throws IOException{
		try {
			int count = client.constructInterface(CustomCollectionsService.class).getCount("published_status=published");
			Assert.assertTrue(count>0);
		} catch (BadRequestException e) {
			throw new RuntimeException(e.getResponse().readEntity(String.class));
		}
	}
}
