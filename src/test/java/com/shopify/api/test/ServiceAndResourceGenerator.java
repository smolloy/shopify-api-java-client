package com.shopify.api.test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;

import com.google.common.base.CaseFormat;

public class ServiceAndResourceGenerator {

	
	//@Test
	public void generateResources() throws IOException{
		
		File outputDir = new File("C:\\Users\\smolloy\\git\\scorehere\\ScoreHere Local GIT Repo\\shopify-api-client\\src\\main\\java\\com\\shopify\\api\\resources\\");
		
		//fetch all the files that are *.java from C:\Users\smolloy\git\scorehere\ScoreHere Local GIT Repo\shopify-api-client\src\main\java\com\shopify\api\resources
		File javaSrcFilesDir = new File("C:\\Users\\smolloy\\git\\scorehere\\ScoreHere Local GIT Repo\\shopify-api-client\\src\\templates\\old-classes");
		Collection<File> files = FileUtils.listFiles(javaSrcFilesDir, new String[]{"java"}, false);
		
		//for each MG* file, find the lines: set(.*?)\((\w+) -> group 1 == name, group 2 == type
		for(File file : files){
			if(file.getName().startsWith("MG")) {
				String fileContent = FileUtils.readFileToString(file);
				
				String className = StringUtils.substringBefore(StringUtils.removeStart(file.getName(), "MG"), ".");
				
				List<String> classLines = new ArrayList<String>();
				boolean hasList = false;
				
				classLines.add("@Getter @Setter @Accessors(chain=true)");
				classLines.add("@JsonRootName(\""+CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, className)+"\")");
				classLines.add("public class "+className+" extends ShopifyResource {");
				
				Matcher setters = Pattern.compile("set(.*?)\\(([^ ]+) .*?\\) \\{").matcher(fileContent);
				while(setters.find()){
					
					String fieldName = StringUtils.uncapitalize(setters.group(1));
					String type = setters.group(2);
					
					if(type.startsWith("List")) hasList=true;
					
					classLines.add("private "+type+" "+fieldName+";");
					
				}
				
				classLines.add("}");
				
				//now add the stuff at the top
				classLines.add(0, "package com.shopify.api.resources;");
				classLines.add(1, "import lombok.Getter;");
				classLines.add(2, "import lombok.Setter;");
				classLines.add(2, "import lombok.experimental.Accessors;");
				classLines.add(3, "import com.shopify.api.common.ShopifyResource;");
				classLines.add(4, "import org.codehaus.jackson.map.annotate.JsonRootName;");
				if(hasList) classLines.add(5, "import java.util.List;");
				
				//output the class file
				FileUtils.writeLines(new File(outputDir, className+".java"), classLines);
				
				System.out.println("Created resource class "+className);
			}
		}
	}
	
	//@Test
	public void createServices() throws IOException{
		String template = FileUtils.readFileToString(new File("C:\\Users\\smolloy\\git\\scorehere\\ScoreHere Local GIT Repo\\shopify-api-client\\src\\templates\\Service.java.template"));
		
		File outputDir = new File("C:\\Users\\smolloy\\git\\scorehere\\ScoreHere Local GIT Repo\\shopify-api-client\\src\\main\\java\\com\\shopify\\api\\services");
	
		String[] services = new String[]{"Article", "Asset", "Blog", "Collect", "Comment", "Country", "CustomCollection", "CustomerGroup", "Customer", 
				"Event", "Fulfillment", "Metafield", "Order", "Page", "Image", "ProductSearchEngine", "Product", 
				"Province", "Redirect", "ScriptTag", "Shop", "SmartCollection", "Theme", "Transaction", "Webhook"};
		
		//{$resourceNameSingle} {$resourceNamePlural} {$resourceNameSingleLower} {$resourceNamePluralLower} {$resourceNamePluralLowerUnderscore}
		
		for(String serviceName : services){
			String serviceClass = template;
			serviceClass = StringUtils.replace(serviceClass, "{$resourceNameSingle}", serviceName);
			serviceClass = StringUtils.replace(serviceClass, "{$resourceNamePlural}", pluralize(serviceName));
			serviceClass = StringUtils.replace(serviceClass, "{$resourceNameSingleLower}", StringUtils.uncapitalize(serviceName));
			serviceClass = StringUtils.replace(serviceClass, "{$resourceNamePluralLower}", pluralize(StringUtils.uncapitalize(serviceName)));
			serviceClass = StringUtils.replace(serviceClass, "{$resourceNamePluralLowerUnderscore}", CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, pluralize(StringUtils.uncapitalize(serviceName))));
			FileUtils.write(new File(outputDir, pluralize(serviceName)+"Service.java"), serviceClass);
			System.out.println("Created service class "+pluralize(serviceName)+"Service");
		}
	}
	
	private String pluralize(String value){
		return value.endsWith("y") ? StringUtils.removeEnd(value, "y")+"ies" : value+"s";
	}
}
