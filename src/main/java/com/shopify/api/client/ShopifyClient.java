package com.shopify.api.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.client.ClientResponseContext;
import javax.ws.rs.client.ClientResponseFilter;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.ext.ContextResolver;

import lombok.extern.slf4j.Slf4j;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.DeserializationConfig.Feature;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.PropertyNamingStrategy;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.map.introspect.AnnotatedClass;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;
import org.codehaus.jackson.type.JavaType;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.filter.HttpBasicAuthFilter;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;

import com.shopify.api.common.BadShopifyRequest;
import com.shopify.api.common.BaseShopifyService;
import com.shopify.api.common.Credential;

@Slf4j
public class ShopifyClient {
	private WebTarget target;
	
	//this will apply the HTTP BASIC auth header for us
//	public static class ApiCredsFilter implements ClientRequestFilter {
//		public void filter(ClientRequestContext req) {
//			
//			req.getHeaders().remove("Accept-Encoding"); //turn gzip compression off while we test
//			
//			UsernamePasswordCredentials creds = new UsernamePasswordCredentials(req.getConfiguration().getProperty("username").toString(), req.getConfiguration().getProperty("password").toString());
//			Header auth = BasicScheme.authenticate(creds, "US-ASCII", false);
//			req.getHeaders().add(auth.getName(), auth.getValue());
//		}
//	}
	
	public static class FunkyPathFixer implements ClientRequestFilter {
		public void filter(ClientRequestContext req) {
			try {
				//this is required because there are cases where the path will eval to "/admin/collects/.json" and should be "/admin/collects.json" for Shopify
				//and because of the bug in RestEasy: https://issues.jboss.org/browse/RESTEASY-677 where @Encoded is being ignored
				req.setUri(new URI(req.getUri().toString().replace("/.", ".").replace(".json%3F", ".json?"))); 
			} catch (URISyntaxException e) {
				throw new RuntimeException(e);
			}
		}
	}
	
	//this is the container for all the serialize/deserialize configs
	public static class JacksonContextResolver implements ContextResolver<ObjectMapper> {
	    private ObjectMapper objectMapper;
	    
	    //this is needed because void return functions appear to explode :(
	    private ObjectMapper voidMapper = new ObjectMapper(){
    		@Override
    		public <T> T readValue(JsonParser jp, JavaType valueType) throws IOException, JsonParseException, JsonMappingException {
    			return null;
    		}
    	};
	    
		public JacksonContextResolver() throws Exception {
	        this.objectMapper = new ObjectMapper()
	        
				//this keeps the future compatibility open
				.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				
				//this is what makes those goofy "first level" object names like: {"custom_collection":{"title":"Test 123"}}
				.configure(Feature.UNWRAP_ROOT_VALUE, true)
				.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true) 
				
				//this keeps our output small
				.configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, false)
				.configure(SerializationConfig.Feature.WRITE_EMPTY_JSON_ARRAYS, false)
				.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false)
				.setSerializationInclusion(Inclusion.NON_NULL) 
				
				//this is the CRAZY shit I have to do to support returning "{"count":184}" as an int
				.setAnnotationIntrospector(new JacksonAnnotationIntrospector(){
					@Override
					public String findRootName(AnnotatedClass ac) {
						if(ac.getGenericType().equals(int.class)) return "count";
						return super.findRootName(ac);
					}
				})
				
				//this is so I can have nice java camel-case, but map to the underscore names in Shopify
				.setPropertyNamingStrategy(PropertyNamingStrategy.CAMEL_CASE_TO_LOWER_CASE_WITH_UNDERSCORES);
	    }
	    public ObjectMapper getContext(Class<?> objectType) {
	    	
	    	if(objectType.equals(Void.TYPE)) {
	    		return voidMapper;
		    }
	    	
	        return objectMapper;
	    }
	}
	
	//to ACTUALLY throttle calls
	public static class ApiCallThrottler implements ClientRequestFilter {
		public void filter(ClientRequestContext req) {
			if(ApiCallThrottleFeedback.atLimit()){
				//wait a half-second here for the bucket to leak a request
				try {
					log.warn("Slowing WAY down, we are AT our limits...");
					Thread.sleep(5000);
				} catch (InterruptedException e) {} //okay so we just go now!
			}
			if(ApiCallThrottleFeedback.nearLimit()){
				//wait a half-second here for the bucket to leak a request
				try {
					log.info("Slowing down, we are near our limits...");
					Thread.sleep(1000);
				} catch (InterruptedException e) {} //okay so we just go now!
			}
		}
	}
	
	//to watch the throttle feedback from previous calls
	public static class ApiCallThrottleFeedback implements ClientResponseFilter {

		public static volatile int currentCallCount = 0;
		public static volatile int totalCallLimit = 40;
		
		public static boolean atLimit(){
			return currentCallCount+3>=totalCallLimit;
		}
		
		public static boolean nearLimit(){
			return currentCallCount+10>=totalCallLimit;
		}
		
		@Override
		public void filter(ClientRequestContext req, ClientResponseContext resp) throws IOException {
			
			String callLimit = resp.getHeaderString("X-Shopify-Shop-Api-Call-Limit");
			
			if(resp.getStatus()==429){
				//woah! hold your horses!
				throw new RuntimeException("Exceeded Shopify Call Speed Limit! "+callLimit);
			}
			
			if(resp.getStatus()==422){
				//woah! bad request!
				throw new BadShopifyRequest("Shopify Request Rejected! "+IOUtils.toString(resp.getEntityStream()));
			}
			
			if(callLimit==null) return; //nothing to see here
			
			//set the values we got back from the server
			currentCallCount = Integer.parseInt(callLimit.split("/")[0]);
			totalCallLimit = Integer.parseInt(callLimit.split("/")[1]);
		}
		
	}
	
	public ShopifyClient(Credential creds) {
		createTarget(creds.getShopName(), creds.getApiKey(), creds.getPassword());
	}
	
	private void createTarget(String shopname, String username, String password){
		
		Client client = ClientBuilder.newClient()
				
			//turn on the Jackson feature
			.register(JacksonFeature.class)
				
			//map all the various filters we need to comply to Shopify wonky-land
			.register(JacksonContextResolver.class)
			.register(new HttpBasicAuthFilter(username, password))
			.register(FunkyPathFixer.class)
			.register(ApiCallThrottler.class) 
			.register(ApiCallThrottleFeedback.class)
			
			//read by ApiCredsFilter
			.property("username", username) 
			.property("password", password)
		
			.property(ClientProperties.CONNECT_TIMEOUT, 1000)
		    .property(ClientProperties.READ_TIMEOUT,    10000);
		
		//this will log the request
		if(log.isDebugEnabled())
			client.register(new LoggingFilter(Logger.getLogger("HTTP"), true));
			
		target = client.target("https://"+shopname+".myshopify.com/");
		
	}

	public <T extends BaseShopifyService> T constructInterface(Class<T> interfaze){
        return WebResourceFactory.newResource(interfaze, target);
	}

}
