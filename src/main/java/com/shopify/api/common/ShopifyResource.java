package com.shopify.api.common;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.joda.time.DateTime;

import com.shopify.api.resources.Metafield;

@Getter @Setter @Accessors(chain=true)
public abstract class ShopifyResource {
	private Long id;
	private DateTime createdAt;
	private DateTime updatedAt;
	private List<Metafield> metafields = new ArrayList<Metafield>();
	
}
