package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import java.util.List;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("order")
public class Order extends ShopifyResource {
private BillingAddress billingAddress;
private String browserIp;
private boolean buyerAcceptsMarketing;
private String cancelReason;
private String cancelledAt;
private String closedAt;
private String currency;
private Customer customer;
private String email;
private String financialStatus;
private String fulfillmentStatus;
private String gateway;
private String landingSite;
private String landingSiteRef;
private List<LineItem> lineItems;
private String name;
private String note;
private List<NoteAttribute> noteAttributes;
private int number;
private int orderNumber;
private PaymentDetails paymentDetails;
private String referringSite;
private ShippingAddress shippingAddress;
private List<ShippingLine> shippingLines;
private String subtotalPrice;
private List<TaxLine> taxLines;
private boolean taxesIncluded;
private String token;
private String totalDiscounts;
private String totalLineItemsPrice;
private String totalPrice;
private String totalTax;
private int totalWeight;
}
