package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("article")
public class Article extends ShopifyResource {
private String author;
private int blogId;
private String bodyHtml;
private String publishedAt;
private String summaryHtml;
private String tags;
private String title;
private int userId;
}
