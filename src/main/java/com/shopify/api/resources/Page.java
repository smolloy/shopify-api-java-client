package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("page")
public class Page extends ShopifyResource {
private String author;
private String bodyHtml;
private String handle;
private String publishedAt;
private int shopId;
private String templateSuffix;
private String title;
}
