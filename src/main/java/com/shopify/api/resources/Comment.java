package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("comment")
public class Comment extends ShopifyResource {
private int articleId;
private String author;
private int blogId;
private String body;
private String bodyHtml;
private String email;
private String ip;
private String publishedAt;
private String status;
private String userAgent;
}
