package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("transaction")
public class Transaction extends ShopifyResource {
private String amount;
private String authorization;
private String kind;
private int orderId;
private Receipt receipt;
private String status;
}
