package com.shopify.api.resources;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.shopify.api.common.ShopifyResource;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("variant")
public class Variant extends ShopifyResource {
private long productId;
private String compareAtPrice;
private String fulfillmentService;
private int grams;
private String inventoryManagement;
private String inventoryPolicy;
private int inventoryQuantity;
private String option1;
private String option2;
private String option3;
private long position;
private BigDecimal price;
private boolean requiresShipping;
private String sku;
private boolean taxable;
private String title;
private String barcode;
}
