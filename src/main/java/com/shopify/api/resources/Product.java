package com.shopify.api.resources;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.codehaus.jackson.map.annotate.JsonRootName;
import org.joda.time.DateTime;

import com.shopify.api.common.ShopifyResource;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("product")
public class Product extends ShopifyResource {
	private String bodyHtml;
	private String handle;
	private Image image;
	private List<Image> images;
	private List<Option> options;
	private String productType;
	//private Boolean published;
	private DateTime publishedAt;
	private String tags;
	private String templateSuffix;
	private String title;
	private List<Variant> variants;
	private String vendor;
	private String publishedScope;
	
	public boolean isPublished(){
		return "global".equalsIgnoreCase(publishedScope) && publishedAt!=null;
	}
	
	public Product setPublished(boolean publish){
		return this.setPublishedScope(publish ? "global" : "none").setPublishedAt(publish ? DateTime.now() : null);
	}
}
