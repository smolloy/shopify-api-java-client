package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("tax_line")
public class TaxLine extends ShopifyResource {
private String price;
private double rate;
private String title;
}
