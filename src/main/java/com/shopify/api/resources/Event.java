package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("event")
public class Event extends ShopifyResource {
private String body;
private String message;
private int subjectId;
private String subjectType;
private String verb;
}
