package com.shopify.api.resources;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.shopify.api.common.ShopifyResource;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("fulfillment")
public class Fulfillment extends ShopifyResource {
private List<LineItem> lineItems;
private Long orderId;
private Receipt receipt;
private String status;
private String trackingCompany;
private String trackingNumber;
}
