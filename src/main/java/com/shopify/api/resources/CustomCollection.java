package com.shopify.api.resources;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.codehaus.jackson.map.annotate.JsonRootName;
import org.joda.time.DateTime;

import com.shopify.api.common.ShopifyResource;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("custom_collection")
public class CustomCollection extends ShopifyResource {
private String bodyHtml;
private String handle;
private String publishedScope;
private DateTime publishedAt;
private String sortOrder;
private String templateSuffix;
private String title;

	public boolean isPublished(){
		return "global".equalsIgnoreCase(publishedScope) && publishedAt!=null;
	}
	
	public CustomCollection setPublished(boolean publish){
		return this.setPublishedScope(publish ? "global" : "none").setPublishedAt(publish ? DateTime.now() : null);
	}
}
