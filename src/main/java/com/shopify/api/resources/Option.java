package com.shopify.api.resources;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.shopify.api.common.ShopifyResource;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("option")
public class Option extends ShopifyResource {
private String name;
private long position;
private long productId;
}
