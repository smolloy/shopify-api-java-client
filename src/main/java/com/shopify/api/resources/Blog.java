package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("blog")
public class Blog extends ShopifyResource {
private String commentable;
private String feedburner;
private String feedburnerLocation;
private String handle;
private String templateSuffix;
private String title;
}
