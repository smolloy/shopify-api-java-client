package com.shopify.api.resources;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

import com.shopify.api.common.ShopifyResource;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("shop")
public class Shop extends ShopifyResource {
private String address1;
private String city;
private String country;
private String currency;
private String domain;
private String email;
private String moneyFormat;
private String moneyWithCurrencyFormat;
private String myshopifyDomain;
private String name;
private String phone;
private String planName;
private String province;
@JsonProperty("public") private boolean isPublic;
private String shopOwner;
private String source;
private String taxShipping;
private String taxesIncluded;
private String timezone;
private String zip;
}
