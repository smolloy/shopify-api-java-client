package com.shopify.api.resources;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.shopify.api.common.ShopifyResource;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("collect")
public class Collect extends ShopifyResource {
private Long collectionId;
private Boolean featured;
private Long position;
private Long productId;
private String sortValue;
}
