package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import java.util.List;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("smart_collection")
public class SmartCollection extends ShopifyResource {
private String bodyHtml;
private String handle;
private String publishedAt;
private List<Rule> rules;
private String sortOrder;
private String templateSuffix;
private String title;
private String publishedScope;
}
