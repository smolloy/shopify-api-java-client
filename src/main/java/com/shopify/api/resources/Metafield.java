package com.shopify.api.resources;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.codehaus.jackson.map.annotate.JsonRootName;

import com.shopify.api.common.ShopifyResource;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("metafield")
public class Metafield extends ShopifyResource {
private String description;
private String key;
private String namespace;
private String value;
private String valueType;
}
