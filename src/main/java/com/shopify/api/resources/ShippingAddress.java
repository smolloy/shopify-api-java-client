package com.shopify.api.resources;
import lombok.Getter;
import lombok.experimental.Accessors;
import com.shopify.api.common.ShopifyResource;
import org.codehaus.jackson.map.annotate.JsonRootName;
import lombok.Setter;
@Getter @Setter @Accessors(chain=true)
@JsonRootName("shipping_address")
public class ShippingAddress extends ShopifyResource {
private String address1;
private String address2;
private String city;
private String company;
private String country;
private String countryCode;
private String firstName;
private String lastName;
private String latitude;
private String longitude;
private String name;
private String phone;
private String province;
private String provinceCode;
private String zip;
}
